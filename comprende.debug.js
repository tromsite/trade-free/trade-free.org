let fallbackLocale = 'en';

var Comprende = function (translationTable, locale, fallbackLocale) {
    return {
        locale: locale,
        fallbackLocale: fallbackLocale ? fallbackLocale : 'en',
        translationTable: translationTable,
        translatePage: function () {
            let translationTable = this.translationTable;
            if (!translationTable) return;

            let locale = this.locale;
            if (!locale || locale === 'en') return;

            $.each(translationTable, function (selector, options) {
                $(selector).each(function () {
                    let it = $(this);

                    if (options['text']) {
                        let substituteValue = options['text'][locale];
                        if (substituteValue) it.html(substituteValue);
                    }

                    if (it.prop('localName') === 'a' && options['link']) {
                        let value = it.attr('href');
                        if (value !== undefined) {
                            let substituteValue = options['link'][locale];
                            if (substituteValue) it.prop('href', substituteValue);
                        }
                    }
                });
            });
        }
    };
};

var comprende = Comprende(translationTable, translationLocale, fallbackLocale);

$(function () {
    comprende.translatePage();
});
