/**
 * Select locale is currently used to translate in LCID form.
 * Default page text is 'en'.
 *
 * See the list of LCIDs to find appropriate for your language:
 * https://wiki.freepascal.org/Language_Codes
 *
 * In general case use simple LCID like:
 *     'de' - for German
 *     'es' - for Spanish
 * In special cases, as for different dialects, use full LCID to
 * differentiate the translation:
 *     'zh-cn' - for China Chinese
 *     'zh-hk' - for Honk Kong SAR Chinese
 */
var translationLocale = 'en';
